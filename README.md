# lazy-brick

#### 介绍

#### 软件架


#### 安装教程


1.  composer require --dev whf/lazy-brick:dev-master
2.  php artisan vendor:publish --provider="whf\lazy-brick\LazyBrickServiceProvider"
3.  xxxx

#### 使用说明

1.  创建model
    php artisan make:model Http/Models/Address
2.  新建Service
    php artisan lb:make_s AddressService --model=App\Http\Models\Address --force=1
3.  新建Controller
    php artisan lb:make_c AddressController --model=App\Http\Models\Address --force=1
4.  生成請求類
    php.exe artisan lb:make_r app\Http\Controllers\Wap AddressController add

#### 参与贡献



#