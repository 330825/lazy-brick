<?php
return [
    'options' => [],

    'route' => [

        'prefix' => env('LB_ROUTE_PREFIX', 'lazy-brick'),
        'namespace' => [
            'service'=>'App\\Http\\Services',
            'controller'=>'App\\Http\\Controllers',
            'default'=>'App\\Http\\Controllers',
        ]
//        'middleware' => ['web', 'admin'],
    ],
];