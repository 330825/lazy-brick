<?php

namespace Whf\LazyBrick;

use App\Http\Transformers\Transformer;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;
use Whf\LazyBrick\Console\RequestCommand;
use Whf\LazyBrick\Console\ResourceGenerator;
use Whf\LazyBrick\Console\ControllerCommand;
use Whf\LazyBrick\Console\ServiceCommand;
use Whf\LazyBrick\Console\TransformerCommand;
use Whf\LazyBrick\Console\AbCommand;


class LazyBrickServiceProvider extends ServiceProvider /*implements DeferrableProvider*/
{
    protected $commands = [
        TransformerCommand::class,
        ControllerCommand::class,
        ServiceCommand::class,
        RequestCommand::class,

    ];
    protected $namespace = 'Whf\LazyBrick';

    /**
     * 服务提供者加是否延迟加载.
     * @var bool
     */
//    protected $defer = true; // 延迟加载服务

    public static function loadClassLoader($class)
    {
        if ('Whf\LazyBrick\Extra\ResourceGenerator' === $class) {
            require __DIR__ . '/extra/ResourceGenerator.php';
        }
    }
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->commands($this->commands);
        $this->app->bind(ResourceGenerator::class);
        // 单例绑定服务
        /*$this->app->singleton(LazyBrick::class, function ($app) {
            return new LazyBrick($app['session'], $app['config']);
        });*/
    }


    /**
     * 暂定
     */
    public function map()
    {
        //
    }



    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/views', 'LazyBrick'); // 视图目录指定
        $this->publishes([
            __DIR__ . '/views' => base_path('resources/views/vendor/lazy-brick'),  // 发布视图目录到resources 下
            __DIR__ . '/config/lazy_brick.php' => config_path('lazy-brick.php'), // 发布配置文件到 laravel 的config 下
        ]);
    }
    /**
      * Get the services provided by the provider.
      * @return array
      */
     /*public function provides()
     {
         // 因为延迟加载 所以要定义 provides 函数 具体参考laravel 文档
         return [LazyBrick::class];
     }*/
}
