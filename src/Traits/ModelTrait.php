<?php

namespace Whf\LazyBrick\Traits;


use Illuminate\Database\Eloquent\Model;

/**
 * 全局返回
 * Class Traits
 * @package App\Http\Traits
 */
trait ModelTrait
{

    /**
     * @param $model
     * @return bool
     * @author wuhangfei <wuhangfei1314@163.com>
     * @date 2020/7/16 17:34
     */
    public function modelExists($model){

        if(empty($model)){
            return false;
        }
        return class_exists($model) && is_subclass_of($model,Model::class);
    }

    /**
     * @param $model
     * @author wuhangfei <wuhangfei1314@163.com>
     * @date 2020/7/16 17:34
     */
    public function formatModelName($model){
        if(empty($model)){
            return false;
        }
        $modelArr=lb_uc_array(explode("\\",str_replace(["/",".","//","_"],"\\",$model)));
        return implode("\\",$modelArr);
    }

}
