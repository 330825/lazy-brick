<?php
/**
 * Created by PhpStorm.
 * User: Minton
 * Email: huobingzeng24@gamil.com
 * Date: 2018/1/26
 * Time: 下午10:01
 * 公共函数
 */


/////////////////////////////////
///
///
/// 函数请使用蛇形命名
///
///
/////////////////////////////////


/**
 * 调试打印信息函数
 */
if (!function_exists('df')) {
    function df(...$vars)
    {
        $file = storage_path() . '/logs/debug.log';

        if (!file_exists($file)) {
            $handle = @fopen($file, "wb");
            @fwrite($handle, 'init debug.log');
        }
        $debug = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
        $debug = array_shift($debug);

        $time = date('Y-m-d H:i:s');
        $message = "\ndebug.info start:\n[{$time}] {$debug['file']}\n#line={$debug['line']}:Debug trace\n";
        $cont = 0;
        foreach ($vars as $var) {
            $message .= "#{$cont} ";
            $message .= var_export($var, true) . "\n";
            $cont++;
        }
        error_log($message . "\n", 3, $file);
    }
}


/**
 * _ 转 大写
 * @param $string
 * @eg.AppAdminModelsUser
 */
if (!function_exists('lb_uc_words')) {
    function lb_uc_words($words,$separator='_') {
        $words = str_replace($separator," ",strtolower($words));
        return str_replace(" ", "", ucwords($words));
    }
}
/**
 * 数组首字母
 * @param $array
 */
if (!function_exists('lb_uc_array')) {
    function lb_uc_array($arr)
    {
        array_walk_recursive($arr, function (&$value, $key) {
            $value = ucfirst(strtolower($value));
        });
        return $arr;
    }
}


