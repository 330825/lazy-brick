<?php
namespace Whf\LazyBrick\Facades;
use Illuminate\Support\Facades\Facade;
class LazyBrick extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'LazyBrick';
    }
}