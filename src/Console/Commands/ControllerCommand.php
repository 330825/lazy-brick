<?php

namespace Whf\LazyBrick\Console;

use Illuminate\Console\GeneratorCommand;
use Whf\LazyBrick\Traits\ModelTrait;

class ControllerCommand extends GeneratorCommand
{
    use ModelTrait;
    /**
     * The name and signature of the console command.
     * @var string
     */
    protected $signature = 'lb:make_c {name}
        {--model=}
        {--module=}
        {--stub=}
        {--force=}
        {--namespace=}';

    /**
     * The console command description.
     * @var string
     */
    protected $description = 'make controller';

    /**
     * @var ResourceGenerator
     */
    protected $generator;

    /**
     * Execute the console command.
     * @author whf <wuhangfei1314@163.com>
     * @date 2019/12/16 23:03
     * @return bool|void|null
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle()
    {
        $modelName = $this->option('model');
        //格式化模型输入,首字母大写
        $modelName=$this->formatModelName($modelName);
        if(!$this->modelExists($this->formatModelName($modelName)))
            return $this->error("Model not exists...");
        $this->generator = new ResourceGenerator($modelName);

        $name = $this->qualifyClass($this->getNameInput());
        $path = $this->getPath($name);

        //文件是否存在验证逻辑
        if ((! $this->hasOption('force') ||
                ! $this->option('force')) &&
            $this->alreadyExists($this->getNameInput())) {
            $this->error($this->getNameInput().' already exists!');
            return false;
        }

        $this->makeDirectory($path);

        $this->files->put($path, $this->buildClass($name));

        $this->info($this->type.' created successfully.');
    }

    /**
     * Get the default namespace for the class.
     * 重写父类的方法，当父类中调用$this->getDefaultNamespace()会执行此函数
     * 编辑器ctrl+左键 会指向父类的方法 要注意
     * @author whf <wuhangfei1314@163.com>
     * @date 2019/12/16 23:03
     * @param string $rootNamespace
     * @return array|bool|string|null
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        if ($namespace = $this->option('namespace')) {
            return $namespace;
        }
        return config("lazy-brick.namespace.controller")
               ."\\".config("lazy-brick.module.controller");

    }

    /**
     * Replace the namespace for the given stub.
     * @author whf <wuhangfei1314@163.com>
     * @date 2019/12/16 23:05
     * @param string $stub
     * @param string $name
     * @return $this|GeneratorCommand
     */
    protected function replaceNamespace(&$stub, $name)
    {
        $stub = str_replace(
            ['DummyNamespace',
                    'DummyServiceNamespace',
                    'DummyModelNamespace'],
            [$this->getNamespace($name),
                    $this->getServiceNamespace(),
                    $this->getModelNamespace()],
            $stub
        );
        return $this;
    }

    protected function getServiceNamespace(){
        return config("lazy-brick.namespace.service")
            ."\\".config("lazy-brick.module.service");
    }

    protected function getModelNamespace(){
        return $this->option('model');
    }

    /**
     * Get the stub file for the generator.
     * @author whf <wuhangfei1314@163.com>
     * @date 2019/12/16 23:06
     * @return array|bool|string|null
     */
    protected function getStub()
    {
        if($stub = $this->option('stub')){
            return $stub;
        }
        return __DIR__."/stubs/controller.stub";
    }

    /**
     * @author whf <wuhangfei1314@163.com>
     * @date 2019/12/16 23:01
     * @param string $stub
     * @param string $name
     * @return mixed|string
     */
    protected function replaceClass($stub, $name)
    {
        return str_replace([
            'DummyClass',
            'DummyServiceName',
        ],
        [
            str_replace($this->getNamespace($name).'\\', '', $name),
            config("lazy-brick.module.controller")."\\".basename($this->getModelNamespace()),
            ],
        $stub
        );
    }

    /**
     * 配置左侧间距，我喜欢用制表符，默认2个
     * @author whf <wuhangfei1314@163.com>
     * @date 2019/12/16 23:06
     * @param $code
     * @param $multiplier
     * @return string
     */
    protected function indentCodes($code,$multiplier=2){
        $indent = str_repeat("\t", $multiplier);
        return rtrim($indent.preg_replace("/\r\n/","\r\n{$indent}",$code));
    }

}
