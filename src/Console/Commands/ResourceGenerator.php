<?php

namespace Whf\LazyBrick\Console;

use Illuminate\Database\Eloquent\Model;

class ResourceGenerator
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * @var array
     */
    protected $formats = [
        'form_field'  => "\$form->%s('%s', __('%s'))",
        'show_field'  => "\$show->field('%s', __('%s'))",
        'grid_column' => "\$grid->column('%s', __('%s'))",
        'input_field' => "\$%s = \$request->input('%s');",
        'condition_field' =>"if(\$%s){\r\n\t\$condition['%s'] = $%s;\r\n}",
        'add_field' => "\$this->mDummyModelName->%s = \$request->input('%s');",
        'update_field' => "\$this->mDummyModelName->%s = \$request->input('%s');",
        'request_rules_field' => "'%s' => '%s',",
        'request_message_field' => "'%s.required' => '请填写%s',",
        'params_field' => "public \$%s = '';"
    ];

    /**
     * @var array
     */
    private $doctrineTypeMapping = [
        'string' => [
            'enum', 'geometry', 'geometrycollection', 'linestring',
            'polygon', 'multilinestring', 'multipoint', 'multipolygon',
            'point',
        ],
    ];

    /**
     * @var array
     */
    protected $fieldTypeMapping = [
        'ip'       => 'ip',
        'email'    => 'email|mail',
        'password' => 'password|pwd',
        'url'      => 'url|link|src|href',
        'mobile'   => 'mobile|phone',
        'color'    => 'color|rgb',
        'image'    => 'image|img|avatar|pic|picture|cover',
        'file'     => 'file|attachment',
    ];

    /**
     * ResourceGenerator constructor.
     *
     * @param mixed $model
     */
    public function __construct($model)
    {
        $this->model = $this->getModel($model);
    }

    /**
     * @param mixed $model
     *
     * @return mixed
     */
    protected function getModel($model)
    {
        if ($model instanceof Model) {
            return $model;
        }

        if (!class_exists($model) || !is_string($model) || !is_subclass_of($model, Model::class)) {
            throw new \InvalidArgumentException("Invalid model [$model] !");
        }
        return new $model();
    }

    /**
     * @return string
     */
    public function generateForm()
    {
        $reservedColumns = $this->getReservedColumns();

        $output = '';

        foreach ($this->getTableColumns() as $column) {
            $name = $column->getName();
            if (in_array($name, $reservedColumns)) {
                continue;
            }
            $type = $column->getType()->getName();
            $default = $column->getDefault();

            $defaultValue = '';

            // set column fieldType and defaultValue
            switch ($type) {
                case 'boolean':
                case 'bool':
                    $fieldType = 'switch';
                    break;
                case 'json':
                case 'array':
                case 'object':
                    $fieldType = 'text';
                    break;
                case 'string':
                    $fieldType = 'text';
                    foreach ($this->fieldTypeMapping as $type => $regex) {
                        if (preg_match("/^($regex)$/i", $name) !== 0) {
                            $fieldType = $type;
                            break;
                        }
                    }
                    $defaultValue = "'{$default}'";
                    break;
                case 'integer':
                case 'bigint':
                case 'smallint':
                case 'timestamp':
                    $fieldType = 'number';
                    break;
                case 'decimal':
                case 'float':
                case 'real':
                    $fieldType = 'decimal';
                    break;
                case 'datetime':
                    $fieldType = 'datetime';
                    $defaultValue = "date('Y-m-d H:i:s')";
                    break;
                case 'date':
                    $fieldType = 'date';
                    $defaultValue = "date('Y-m-d')";
                    break;
                case 'time':
                    $fieldType = 'time';
                    $defaultValue = "date('H:i:s')";
                    break;
                case 'text':
                case 'blob':
                    $fieldType = 'textarea';
                    break;
                default:
                    $fieldType = 'text';
                    $defaultValue = "'{$default}'";
            }
            $defaultValue = $defaultValue ?: $default;
            $label = $this->formatLabel($name);

            $output .= sprintf($this->formats['form_field'], $fieldType, $name, $label);

            if (trim($defaultValue, "'\"")) {
                $output .= "->default({$defaultValue})";
            }
            $output .= ";\r\n";
        }
        return $output;
    }

    /**
     * @return string
     * @throws \Exception
     * @author wuhangfei <wuhangfei1314@163.com>
     * @date 2020/7/16 22:38
     * Service 查询公用检索代码
     */
    public function generateCondition()
    {
        $reservedColumns = $this->getReservedColumns();
        $inputString = '';
        $conditionString = '';
        foreach ($this->getTableColumns() as $column) {
            $name = $column->getName();
            if(in_array($name,$reservedColumns)){
                continue;
            }
//            $label = $this->formatLabel($name);
            $label = $name;
            $inputString .= sprintf($this->formats['input_field'], $name, $label);
            $inputString .= "\r\n";
            $conditionString .= sprintf($this->formats['condition_field'], $name,$name, $name);
            $conditionString .= "\r\n";
        }
        return $inputString.$conditionString;
    }

    /**
     * @return string
     * @throws \Exception
     * @author wuhangfei <wuhangfei1314@163.com>
     * @date 2020/7/16 22:38
     * Service 添加Action代码
     */
    public function generateAdd(){
        $reservedColumns = $this->getReservedColumns();
        $output = '';
        foreach ($this->getTableColumns() as $column) {
            $name = $column->getName();
            if(in_array($name,$reservedColumns)){
                continue;
            }
//            $label = $this->formatLabel($name);
            $label = $name;
            $output .= sprintf($this->formats['add_field'], $name, $label);
            $output .= "\r\n";
        }
        return $output;
    }

    /**
     * @return string
     * @throws \Exception
     * @author wuhangfei <wuhangfei1314@163.com>
     * @date 2020/7/16 22:37
     * Service 更新Action代码
     */
    public function generateUpdate(){
        $reservedColumns = $this->getReservedColumns();
        $output = '';
        foreach ($this->getTableColumns() as $column) {
            $name = $column->getName();
            if(in_array($name,$reservedColumns)){
                continue;
            }
//            $label = $this->formatLabel($name);
            $label = $name;
            $output .= sprintf($this->formats['update_field'], $name, $label);
            $output .= "\r\n";
        }
        return $output;
    }

    /**
     * @return string
     * @throws \Exception
     * @author wuhangfei <wuhangfei1314@163.com>
     * @date 2020/7/16 22:37
     * Request filter rule
     */
    public function generateRequestRules(){
        $reservedColumns = $this->getReservedColumns();
        $output = '';
        foreach ($this->getTableColumns() as $column) {
            $name = $column->getName();
            if(in_array($name,$reservedColumns)){
                continue;
            }
            $fieldType=$this->getRequestRules($column->getType()->getName());

            //$label = $this->formatLabel($name);
            $output .= sprintf($this->formats['request_rules_field'], $name, $fieldType);
            $output .= "\r\n";
        }

        return $output;
    }

    /**
     * @return string
     * @throws \Exception
     * @author wuhangfei <wuhangfei1314@163.com>
     * @date 2020/7/16 23:01
     * Request filter attributes
     */
    public function generateRequestAttributes(){
        $reservedColumns = $this->getReservedColumns();
        $output = '';
        foreach ($this->getTableColumns() as $column) {
            $name = $column->getName();
            if(in_array($name,$reservedColumns)){
                continue;
            }
            $output .= sprintf($this->formats['request_rules_field'], $name, $column->getComment());
            $output .= "\r\n";
        }

        return $output;
    }

    /**
     * @return string
     * @throws \Exception
     * @author wuhangfei <wuhangfei1314@163.com>
     * @date 2020/7/16 23:01
     * request filter message
     */
    public function generateRequestMessage(){
        $reservedColumns = $this->getReservedColumns();
        $output = '';
        foreach ($this->getTableColumns() as $column) {
            $name = $column->getName();
            if(in_array($name,$reservedColumns)){
                continue;
            }
            $output .= sprintf($this->formats['request_message_field'], $name, $column->getComment());
            $output .= "\r\n";
        }
        return $output;
    }

    /**
     * @return string
     * @throws \Exception
     * @author wuhangfei <wuhangfei1314@163.com>
     * @date 2020/10/13 1:09
     */
    public function generateParams(){
        $reservedColumns = $this->getReservedColumns();
        $output = '';
        foreach ($this->getTableColumns() as $column) {
            $name = $column->getName();
            if(in_array($name,$reservedColumns)){
                continue;
            }
            $output .= sprintf($this->formats['params_field'], $name);
            $output .= "\r\n";
        }
        return $output;
    }





    protected function getRequestRules($type){

        switch ($type) {
            case 'boolean':
            case 'bool':
                $rules = 'required|string|max:5';
                break;
            case 'array':
            case 'object':
                $rules = 'required';
                break;
            case 'integer':
            case 'bigint':
            case 'smallint':
            case 'timestamp':
                $rules = 'required|numeric';
                break;
            case 'decimal':
            case 'float':
            case 'real':
                $rules = 'required|numeric';
                break;
            case 'datetime':
            case 'date':
            case 'time':
                $rules = 'required|date';
                break;
            case 'text':
            case 'blob':
                $rules = 'required|string';
                break;
            case 'json':
            case 'string':
                $rules = 'required|string|max:255';
                break;
            default:
                $rules = 'required';
        }
        return $rules;
    }



    public function generateShow(){
        $output = '';

        foreach ($this->getTableColumns() as $column) {
            $name = $column->getName();
            $label = $this->formatLabel($name);

            $output .= sprintf($this->formats['show_field'], $name, $label);
            $output .= ";\r\n";
        }

        return $output;
    }

    public function generateGrid()
    {
        $output = '';

        foreach ($this->getTableColumns() as $column) {
            $name = $column->getName();
            $label = $this->formatLabel($name);

            $output .= sprintf($this->formats['grid_column'], $name, $label);
            $output .= ";\r\n";
        }

        if(isset($condition)){
            $this->model::where($condition);
        }
        return $this->model;

        return $output;
    }

    protected function getReservedColumns()
    {
        return [
            $this->model->getKeyName(),
            $this->model->getCreatedAtColumn(),
            $this->model->getUpdatedAtColumn(),
            'deleted_at',
        ];
    }

    /**
     * Get columns of a giving model.
     *
     * @throws \Exception
     *
     * @return \Doctrine\DBAL\Schema\Column[]
     */
    protected function getTableColumns()
    {
        if (!$this->model->getConnection()->isDoctrineAvailable()) {
            throw new \Exception(
                'You need to require doctrine/dbal: ~2.3 in your own composer.json to get database columns. '
            );
        }

        $table = $this->model->getConnection()->getTablePrefix().$this->model->getTable();
        /** @var \Doctrine\DBAL\Schema\MySqlSchemaManager $schema */
        $schema = $this->model->getConnection()->getDoctrineSchemaManager($table);

        // custom mapping the types that doctrine/dbal does not support
        $databasePlatform = $schema->getDatabasePlatform();

        foreach ($this->doctrineTypeMapping as $doctrineType => $dbTypes) {
            foreach ($dbTypes as $dbType) {
                $databasePlatform->registerDoctrineTypeMapping($dbType, $doctrineType);
            }
        }

        $database = null;
        if (strpos($table, '.')) {
            list($database, $table) = explode('.', $table);
        }

        return $schema->listTableColumns($table, $database);
    }

    /**
     * Format label.
     *
     * @param string $value
     *
     * @return string
     */
    protected function formatLabel($value)
    {
        return lb_uc_words($value);
        //return ucfirst(str_replace(['-', '_'], ' ', $value));
    }


}
